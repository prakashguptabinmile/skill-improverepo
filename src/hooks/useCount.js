import {useEffect, useState} from 'react';

const useCount = () => {
  const [count, setCount] = useState(0);
  useEffect(() => {
    if (count >= 1) {
      setCount((count = count + 1));
    } else {
      setCount((count = count - 1));
    }
  }, [count]);
};
export default useCount;
