import React from 'react';
import SomeContext from './SomeContext';
import ImageHome from '../assets/wallpaper1.jpg';
import ImageAbout from '../assets/wallpaper.jpg';
import ImageServices from '../assets/image2.png';

const SomeState = props => {
  const Data = {
    name: 'Prakash Gupta',
    company: 'Binmile Technologies',
    ImageHome: ImageHome,
    ImageAbout: ImageAbout,
    ImageServices: ImageServices,
  };
  return (
    <SomeContext.Provider value={Data}>{props.children}</SomeContext.Provider>
  );
};

export default SomeState;
