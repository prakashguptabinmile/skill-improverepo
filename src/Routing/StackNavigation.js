import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Components/Home';
import About from '../Components/About';
import Services from '../Components/Services';
import SomeState from '../Context/SomeState';

const Stack = createStackNavigator();

const StackNavigation = () => {
  return (
    <>
      <SomeState>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {backgroundColor: '#00BFFF'},
            headerTitleAlign: 'center',
          }}>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="About" component={About} />
          <Stack.Screen name="Services" component={Services} />
        </Stack.Navigator>
      </SomeState>
    </>
  );
};

export default StackNavigation;
