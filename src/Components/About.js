import {TouchableOpacity, Text, ImageBackground} from 'react-native';
import React, {useContext, useState} from 'react';
import SomeContext from '../Context/SomeContext';
import styles from './Styles';
// import useCount from '../hooks/useCount';
// import ReactDOM from 'react-dom';
import useFetch from '../hooks/useFetch';

const About = ({navigation}) => {
  const data = useFetch('https://jsonplaceholder.typicode.com/todos');
  const homeData = useContext(SomeContext);
  const [count, setCount] = useState(0);
  // useCount(count);
  return (
    <ImageBackground
      resizeMode="cover"
      source={homeData.ImageAbout}
      style={styles.imageBG}>
         {data &&
        data.map((item) => {
        <Text key={item.id} >{item.title}</Text>;
        })}
       

       
      <Text>{count}</Text>
      <TouchableOpacity onPress={() => setCount(count + 1)}>
        <Text>Click</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('Services')}
        style={styles.btn}>
        <Text style={styles.btnText}>Go to Services</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default About;
