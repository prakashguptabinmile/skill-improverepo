import {TouchableOpacity, Text, ImageBackground} from 'react-native';
import React, {useContext} from 'react';
import SomeContext from '../Context/SomeContext';
import styles from './Styles';
import {useDispatch, useSelector} from 'react-redux';
import {addition, subtraction} from '../../store/Action';

const Home = ({navigation}) => {
  const homeData = useContext(SomeContext);

  const data = useSelector(state => state.counter);
  const dispatch = useDispatch();
  return (
    <ImageBackground
      resizeMode="cover"
      source={homeData.ImageHome}
      style={styles.imageBG}>
      <Text style={styles.textView}>
        My name is {homeData.name} and I work in {homeData.company}
      </Text>
      <TouchableOpacity
        onPress={() => dispatch(addition())}
        style={styles.counterBtn}>
        <Text style={styles.btnText}>Increase</Text>
      </TouchableOpacity>

      <Text style={styles.btnText}>{data}</Text>

      <TouchableOpacity
        onPress={() => dispatch(subtraction())}
        style={styles.counterBtn}>
        <Text style={styles.btnText}>Decrease</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => navigation.navigate('About')}
        style={styles.btn}>
        <Text style={styles.btnText}>Go to About</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default Home;
