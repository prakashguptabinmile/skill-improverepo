import {View, TouchableOpacity, Text, ImageBackground} from 'react-native';
import React, {useContext, useState, useMemo} from 'react';
import SomeContext from '../Context/SomeContext';
import styles from './Styles';
import {TextInput} from 'react-native-gesture-handler';
import useLocalStorage from '../hooks/useLocalStorage';
import useUpdateLogger from '../hooks/useUpdateLogger';

const Services = ({navigation}) => {
  const homeData = useContext(SomeContext);

  const [name, setName] = useLocalStorage('name', '');
  useUpdateLogger(name);

  const [count, setCount] = useState(0);
  const [item, setItem] = useState(10);
  const multiCountMemo = useMemo(
    function multiCount() {
      console.log('multiCount');
      return count * 5;
    },
    [count],
  );

  return (
    <ImageBackground
      resizeMode="cover"
      source={homeData.ImageServices}
      style={styles.imageBG}>
      <Text style={styles.btnText}>Count: {count}</Text>
      <Text style={styles.btnText}>Items: {item}</Text>
      <Text style={styles.btnText}>{multiCountMemo}</Text>
      <TouchableOpacity
        style={styles.counterBtn}
        onPress={() => setCount(count + 1)}>
        <Text style={styles.btnText}> Count + 1</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.counterBtn}
        onPress={() => setItem(item * 2)}>
        <Text style={styles.btnText}>Update Item</Text>
      </TouchableOpacity>

      <TextInput
        placeholder="Name Here"
        value={name}
        onChange={e => setName(e.target.value)}
      />

      <TouchableOpacity
        onPress={() => navigation.navigate('Home')}
        style={styles.btn}>
        <Text style={styles.btnText}>Go back to Home</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

export default Services;
