import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  containerView: {
    marginHorizontal: 20,
  },
  imageBG: {
    flex: 1,
    justifyContent: 'center',
  },
  textView: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 50,
  },
  btn: {
    backgroundColor: '#00BFFF',
    paddingVertical: 10,
    borderRadius: 15,
    position: 'absolute',
    margin: 20,
    left: 0,
    right: 0,
    bottom: 0,
  },
  btnText: {
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 15,
  },
  counterBtn: {
    marginHorizontal: 20,
    backgroundColor: 'orange',
    paddingVertical: 7,
    borderRadius: 10,
    marginVertical: 4,
  },
});
export default styles;
