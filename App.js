import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import StackNavigation from './src/Routing/StackNavigation';
import SomeState from './src/Context/SomeState';
import {Provider} from 'react-redux';
import {store} from './store/Store';
import 'localstorage-polyfill'; 

const App = () => {
  return (
    <Provider store={store}>
      <SomeState>
        <NavigationContainer>
          <StackNavigation />
        </NavigationContainer>
      </SomeState>
    </Provider>
  );
};

export default App;
